<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'Home';
});

Route::get('usuarios', 'UserController@index');

Route::get('usuario/{id}', 'UserController@show')->where('id','[0-9]+');

Route::get('usuario/nuevo', 'UserController@create');

Route::get('usuario/{id}/edit', 'UserController@edit')->where('id','[0-9]+');

Route::get('saludo/{firstname}', 'WelcomeUserController@greet1');

Route::get('saludo/{firstname}/{lastname}','WelcomeUserController@greet2');


<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserModuleTest extends TestCase
{
    /** @test */
    function it_load_page_with_list_users()
    {
        $this->get('usuarios')
            ->assertStatus(200)
            ->assertSee('Usuarios');
    }

    /** @test */
    function it_load_detail_of_user()
    {
        $this->get('usuario/5')
            ->assertStatus(200)
            ->assertSee('Mostrando detalle del usuario: 5');
    }

    /** @test */
    function it_load_page_for_create_user()
    {
        $this->get('usuario/nuevo')
            ->assertStatus(200)
            ->assertSee('Crear nuevo usuario');
    }

    /** @test */
    function it_load_page_for_edit_user_fail()
    {
        $this->get('usuario/5/edit')
            ->assertStatus(200)
            ->assertSee('Editar información del usuario 5');
    }

}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WelcomeUserTest extends TestCase
{
    /** @test */
    function it_load_page_whit_message_users_with_lastname()
    {
        $this->get('saludo/fabian/triana')
            ->assertStatus(200)
            ->assertSee('Bienvenid@ usuario: Fabian Triana');
    }

    /** @test */
    function it_load_page_with_message_users_without_firstname()
    {
        $this->get('saludo/fabian')
            ->assertStatus(200)
            ->assertSee('Bienvenid@ usuario: Fabian');
    }
}

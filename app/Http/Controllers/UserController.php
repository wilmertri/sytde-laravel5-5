<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = [
            'Jose',
            'Pedro',
            'Lucia',
            'Jaconbo',
            'Antonia',
            '<script>alert("Clicked!");</script>'
        ];

        $title = "Listado de Usuarios";

        return view('users', compact('users', 'title'));
    }

    public function show($id)
    {
        return "Mostrando detalle del usuario: {$id}";
    }

    public function create()
    {
        return 'Crear nuevo usuario';
    }

    public function edit($id)
    {
        return "Editar información del usuario {$id}";
    }
}

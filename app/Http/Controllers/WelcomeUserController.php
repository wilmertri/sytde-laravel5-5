<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeUserController extends Controller
{
    public function greet1($firstname)
    {
        $firstname = ucfirst($firstname);
        return "Bienvenid@ usuario: $firstname";

    }

    public function greet2($firstname, $lastname)
    {
        $firstname = ucfirst($firstname);
        $lastname = ucfirst($lastname);
        return "Bienvenid@ usuario: $firstname $lastname";
    }
}
